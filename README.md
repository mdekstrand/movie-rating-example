# Movie Analysis

This notebook provides an example of analyzing MovieLens rating data in R.

To use, download the ML-20M data set from https://grouplens.org/datasets/movielens/
and extract it into a directory called `data` (you should then have a file
`data/ml-20m/ratings.csv`).
